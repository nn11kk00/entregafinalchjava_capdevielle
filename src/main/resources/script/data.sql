-- Datos para la bd

-- Insertar datos en la tabla "clientes"
INSERT INTO clientes (dni, nombre, apellido, fecha_nacimiento) VALUES ('1234567890', 'Juan', 'Pérez', '1990-05-15');
INSERT INTO clientes (dni, nombre, apellido, fecha_nacimiento) VALUES ('9876543210', 'María', 'Gómez', '1988-09-23');

-- Insertar datos en la tabla "productos"
INSERT INTO productos (descripcion, precio_compra, precio_venta, stock, fecha_alta) VALUES ('Producto A', 10.0, 20.0, 50, '2023-01-01');
INSERT INTO productos (descripcion, precio_compra, precio_venta, stock, fecha_alta) VALUES ('Producto B', 15.0, 25.0, 30, '2023-02-01');

-- Insertar datos en la tabla "ventas"
INSERT INTO ventas (fecha_alta, total, cliente_id) VALUES ('2023-06-01', 40.0, 1);
INSERT INTO ventas (fecha_alta, total, cliente_id) VALUES ('2023-07-01', 50.0, 2);

-- Insertar datos en la tabla "detalles_venta"
INSERT INTO detalles_venta (venta_id, producto_id, cantidad, sub_total) VALUES (1, 1, 2, 40.0);
INSERT INTO detalles_venta (venta_id, producto_id, cantidad, sub_total) VALUES (2, 2, 3, 75.0);

