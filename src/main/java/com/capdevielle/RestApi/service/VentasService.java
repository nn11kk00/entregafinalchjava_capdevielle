package com.capdevielle.RestApi.service;


import com.capdevielle.RestApi.exception.ResourceNotFoundException;
import com.capdevielle.RestApi.model.Cliente;
import com.capdevielle.RestApi.model.Producto;
import com.capdevielle.RestApi.model.Venta;
import com.capdevielle.RestApi.model.VentaDetalle;
import com.capdevielle.RestApi.repository.ClienteRepository;
import com.capdevielle.RestApi.repository.ProductoRepository;
import com.capdevielle.RestApi.repository.VentaDetalleRepository;
import com.capdevielle.RestApi.repository.VentaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;


@Service
public class VentasService {

    @Autowired
    private VentaRepository vr;
    @Autowired
    private ClienteRepository cr;
    @Autowired
    private VentaDetalleRepository vdr;
    @Autowired
    private ProductoRepository pr;

    private RestTemplate restTemplate = new RestTemplate();
    private String apiUrl = "http://worldclockapi.com/api/json/utc/now";

    public Venta create(Venta newVenta) throws ResourceNotFoundException {
        Venta v = new Venta();
        v.setFechaAlta(obtenerFechaAlta());
        Optional<Cliente> cliente = cr.findById(newVenta.getCliente().getId());
        if (cliente.isPresent()) {
            v.setCliente(cliente.get());
        } else {
            throw new ResourceNotFoundException("No existe");
        }
        Venta venta = this.vr.save(v);
        List<VentaDetalle> detalleVenta = generarVentaDetalle(newVenta.getDetalle(),venta);

        Double total = detalleVenta.stream()
                .mapToDouble(VentaDetalle::getSubTotal)
                .sum();
        v.setTotal(total);

        List<VentaDetalle> saveDetalle = this.vdr.saveAll(detalleVenta);
        venta.setDetalle(saveDetalle);
        return venta;
    }

    private LocalDate obtenerFechaAlta() {
        try {
            ResponseEntity<Map> response = restTemplate.getForEntity(apiUrl, Map.class);
            Map<String, Object> responseData = response.getBody();
            if (responseData.containsKey("currentDateTime")) {
                String currentDateTime = responseData.get("currentDateTime").toString();
                String datePart = currentDateTime.substring(0, 10);

                return LocalDate.parse(datePart, DateTimeFormatter.ISO_LOCAL_DATE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return LocalDate.now();
    }









    private List<VentaDetalle> generarVentaDetalle(List<VentaDetalle> detalle, Venta venta) {
        List<VentaDetalle> detalleVenta = new ArrayList<>();
        detalle.forEach(item -> {
            try {
                VentaDetalle detalleItem = createVentaDetalle(item,venta);
                detalleVenta.add(detalleItem);
            } catch (ResourceNotFoundException ex) {
                throw new RuntimeException(ex);
            }
        });
        return detalleVenta;
    }

    public List<Venta> getAllVentas() {
        return vr.findAll();
    }


    private VentaDetalle createVentaDetalle(VentaDetalle newvd,Venta venta) throws ResourceNotFoundException {
        VentaDetalle vd = new VentaDetalle();
        vd.setVenta(venta);
        vd.setCantidad(newvd.getCantidad());
        Optional<Producto> producto = pr.findById(newvd.getProducto().getId());
        if (producto.isPresent()) {
            vd.setProducto(producto.get());
        } else {
            throw new ResourceNotFoundException("No existe");
        }

        vd.setSubTotal(vd.getProducto().getPrecioVenta() * vd.getCantidad());
        return vd;
    }
}
