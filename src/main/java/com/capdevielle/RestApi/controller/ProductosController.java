package com.capdevielle.RestApi.controller;


import com.capdevielle.RestApi.exception.DuplicateProductException;
import com.capdevielle.RestApi.exception.ResourceNotFoundException;
import com.capdevielle.RestApi.model.Producto;
import com.capdevielle.RestApi.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping(path = "api/product")
@RestController
public class ProductosController {

    @Autowired
    private ProductoService ps;

    @PostMapping("/")
    public ResponseEntity<?> create(@RequestBody Producto p) {
        try {
            Producto nuevoProducto = ps.create(p);
            return new ResponseEntity<>(nuevoProducto, HttpStatus.OK);
        } catch (DuplicateProductException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/")
    public ResponseEntity<List<Producto>>finAll(){
        return new ResponseEntity<>(this.ps.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Producto> getProduct(@PathVariable Long id) {
        try {
            Producto producto = ps.getProductById(id);
            return new ResponseEntity<>(producto, HttpStatus.OK);
        } catch (ResourceNotFoundException e) {
            return ResponseEntity.notFound().build();
        }
    }


    @PutMapping(path = "/{id}")
    public ResponseEntity<Producto> update (@RequestBody Producto pUpdate, @PathVariable("id") Long id) throws ResourceNotFoundException {
        return new ResponseEntity<>(this.ps.update(pUpdate, id), HttpStatus.OK);
    }

    @DeleteMapping(path = "/{id}")
    public ResponseEntity<String> delet(@PathVariable("id") Long id){
        ps.delet(id);
        return ResponseEntity.ok("Producto eliminado correctamente");
    }




}
